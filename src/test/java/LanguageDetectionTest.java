import com.procyk.maciej.docsmanagers.DocExtractor;
import com.procyk.maciej.docsmanagers.LanguageDetector;
import com.procyk.maciej.properties.DocLanguage;
import com.procyk.maciej.special.SkipException;
import static org.assertj.core.api.Assertions.*;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.io.InputStream;

public class LanguageDetectionTest {
    @Test
    void testSimpleDetection() throws IOException, SkipException {
        try (InputStream stream = LanguageDetectionTest.class.getResourceAsStream("DOCX.docx")) {
            String content = DocExtractor.parseToString(stream);

            assertThat(content).isNotEmpty();
            assertThat(LanguageDetector.identifyLanguage(content)).isEqualByComparingTo(DocLanguage.EN);
            assertThat(LanguageDetector.identifyLanguage(content)).isNotEqualByComparingTo(DocLanguage.PL);
        }
    }
}
