import com.procyk.maciej.docsmanagers.DocExtractor;
import com.procyk.maciej.special.ParsingException;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.*;

public class FilesExceptionsParsingTest {
    @Test
    void testNullFile() {
        assertThatExceptionOfType(ParsingException.class).isThrownBy(
                () -> DocExtractor.parseToString(null));
    }

    @Test
    void testUnsupportedFile() {
        assertThatExceptionOfType(ParsingException.class).isThrownBy(
                () -> DocExtractor.parseToString(FilesExceptionsParsingTest.class.getResourceAsStream("UNSUPPORTED.html")));
        assertThatExceptionOfType(ParsingException.class).isThrownBy(
                () -> DocExtractor.parseToString(FilesExceptionsParsingTest.class.getResourceAsStream("UNSUPPORTED")));
    }
}
