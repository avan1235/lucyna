import com.procyk.maciej.docsmanagers.DocExtractor;
import static org.assertj.core.api.Assertions.*;

import com.procyk.maciej.special.SkipException;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;


import java.io.IOException;
import java.io.InputStream;


public class FilesParsingTest {

    @ParameterizedTest
    @ValueSource(strings = {"txt", "rtf", "pdf", "docx", "odt"})
    void testParsing(String fileType) throws IOException, SkipException {
        try (
                InputStream streamNormal = FilesParsingTest.class.getResourceAsStream(fileType.toUpperCase() + "." + fileType);
                InputStream streamNoExt = FilesParsingTest.class.getResourceAsStream(fileType.toUpperCase())
        ) {
            assertThat(streamNormal).isNotNull();
            assertThat(streamNoExt).isNotNull();

            String contentNormal = DocExtractor.parseToString(streamNormal);
            String contentNoExt = DocExtractor.parseToString(streamNoExt);

            assertThat(contentNormal).isNotEmpty();
            assertThat(contentNormal).contains(fileType);
            assertThat(contentNormal).containsIgnoringCase("test");

            assertThat(contentNoExt).isNotEmpty();
            assertThat(contentNoExt).contains(fileType);
            assertThat(contentNoExt).containsIgnoringCase("test");
        }
    }
}
