import com.procyk.maciej.output.OutputPathsListFormatter;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Collection;

public class OutputPathsListFormatterTest {
    @Test
    void testEmptyList() {
        Collection<String> c = new ArrayList<>();
        OutputPathsListFormatter formatter = new OutputPathsListFormatter();
        String out = formatter.getFormattedOutput(c);
        Assertions.assertThat(out).doesNotContain("1", "2", "3");
        Assertions.assertThat(out).isEqualTo("0 paths in index" + System.lineSeparator());
    }

    @Test
    void testSingleList() {
        Collection<String> c = new ArrayList<>();
        c.add("1st one");
        OutputPathsListFormatter formatter = new OutputPathsListFormatter();
        String out = formatter.getFormattedOutput(c);
        Assertions.assertThat(out).doesNotContain("0", "2", "3");
        Assertions.assertThat(out).isGreaterThan("1 path in index" + System.lineSeparator());
    }

    @Test
    void testMany() {
        Collection<String> c = new ArrayList<>();
        c.add("1st one");
        c.add("2nd one");
        OutputPathsListFormatter formatter = new OutputPathsListFormatter();
        String out = formatter.getFormattedOutput(c);
        Assertions.assertThat(out).doesNotContain("0", "3");
        Assertions.assertThat(out).isGreaterThan("2 paths in index" + System.lineSeparator());
    }
}
