package com.procyk.maciej.properties;

import java.util.Objects;

/**
 * Enum class for enumeration of supported files types in indexer
 */
public enum SupportedFileType {
    TXT,
    RTF,
    PDF,
    ODF,
    OOXML,
    NOT_SUPPORTED;

    /**
     * Tika based convention of file types description of their types
     * @param description from {@link org.apache.tika.Tika} about the file type
     * @return file type of specified description of file
     */
    public static SupportedFileType getFileType(String description) {
        if (Objects.isNull(description)) return NOT_SUPPORTED;
        else if ("text/plain".equals(description)) return TXT;
        else if ("application/rtf".equals(description)) return RTF;
        else if ("application/pdf".equals(description)) return PDF;
        else if (description.contains("openxml")) return OOXML;
        else if (description.contains("opendocument")) return ODF;
        else return NOT_SUPPORTED;
    }

}
