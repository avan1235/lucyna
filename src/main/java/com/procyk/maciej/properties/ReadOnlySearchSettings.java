package com.procyk.maciej.properties;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.search.Query;

/**
 * Read only version of the {@link SearchSettings} which can
 * be returned from somewhere but not changed elsewhere
 */
public class ReadOnlySearchSettings {

    private final SearchSettings searchSettings;

    public ReadOnlySearchSettings(SearchSettings searchSettings) {
        this.searchSettings = searchSettings;
    }

    public String getContentField() {
        return this.searchSettings.getContentField();
    }

    public String getFileNameField() {
        return this.searchSettings.getFileNameField();
    }

    public Analyzer getLanguageAnalyzer() {
        return this.searchSettings.getLanguageAnalyzer();
    }

    public boolean isDetailed() {
        return this.searchSettings.isDetailed();
    }

    public boolean isColored() {
        return this.searchSettings.isColored();
    }

    public int getLimitOfResults() {
        return this.searchSettings.getLimitOfResults();
    }

    public Query getQuery(String field, Analyzer analyzer, String searchFor) {
        return this.searchSettings.getQuery(field, analyzer, searchFor);
    }

    @Override
    public String toString() {
        return searchSettings.toString();
    }
}
