package com.procyk.maciej.properties;

import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Enum class for definition of available documents parsed languages
 */
public enum DocLanguage {
    PL("pl") {
        @Override
        public String getContentForLanguageFieldName() {
            return LuceneConstants.CONTENT_PL;
        }
        @Override
        public String getFileNameForLanguageFieldName() {
            return LuceneConstants.FILE_NAME_PL;
        }
    },
    EN("en") {
        @Override
        public String getContentForLanguageFieldName() {
            return LuceneConstants.CONTENT_EN;
        }
        @Override
        public String getFileNameForLanguageFieldName() {
            return LuceneConstants.FILE_NAME_EN;
        }
    };

    private final String language;

    /**
     * Get the string with the name of the field with the content for
     * specified language by enum variable
     * @return field name
     */
    public abstract String getContentForLanguageFieldName();

    /**
     * Get the string with the name of the field with the name of the
     * file for the specified language
     * @return field name
     */
    public abstract String getFileNameForLanguageFieldName();

    DocLanguage(String lang) {
        this.language = lang;
    }

    @Override
    public String toString() {
        return this.language;
    }

    /**
     * GEt the stream of available {@link DocLanguage} defined
     * @return stream of languages
     */
    public static Stream<DocLanguage> stream() {
        return Stream.of(DocLanguage.values());
    }

    /**
     * Get the set of the available language names (their descriptions
     * from the constructors) so call for every of them the {@link #toString()}
     * method and collect everything to set
     * @return set of available languages names
     */
    public static Set<String> getAvailableLangsNames() {
        return DocLanguage.stream().map(DocLanguage::toString).collect(Collectors.toSet());
    }
}
