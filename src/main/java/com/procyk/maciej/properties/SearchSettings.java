package com.procyk.maciej.properties;

import com.procyk.maciej.docsmanagers.AnalyzerFactory;
import com.procyk.maciej.special.UnknownLanguageException;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.en.EnglishAnalyzer;
import org.apache.lucene.index.Term;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.FuzzyQuery;
import org.apache.lucene.search.PhraseQuery;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.TermQuery;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;
import java.util.function.BiFunction;
import java.util.function.Function;

/**
 * Class for managing the search setting and interpreting the changes of settings
 */
public class SearchSettings {

    private static Logger logger = LoggerFactory.getLogger(SearchSettings.class);

    private static final BiFunction<String, String, Boolean> stringComparator = String::equalsIgnoreCase;
    private static final Function<String, String> stringChanger = String::toUpperCase;
    private static final String ON = "on";
    private static final String OFF = "off";

    /**
     * Enum of used search types which can be used by changing the search
     * type in settings. every of them has its own method for creating
     * the {@link Query} for searching process
     */
    public enum SearchType {
        TERM {
            @Override
            public Query getQuery(String field, Analyzer analyzer, String searchFor) {
                Query result = new TermQuery(new Term(field, searchFor));
                /* Alternative version for tokenizing the input an getting term/phrase query
                try {
                    QueryParser parser = new QueryParser(field, analyzer);
                    result = parser.parse(searchFor);
                } catch (ParseException exc) {
                    result = new TermQuery(new Term(field, searchFor));
                }
                */
                return result;
            }
        },
        PHRASE {
            @Override
            public Query getQuery(String field, Analyzer analyzer, String searchFor) {
                Query result;
                // in this situation it's better to use parser in order to
                // for example get rid of the endings of the words
                try {
                    QueryParser parser = new QueryParser(field, analyzer);
                    result = parser.parse(searchFor);
                } catch (ParseException exc) {
                    result = new PhraseQuery(field, searchFor.toLowerCase().split("\\s+"));
                }
                return result;
            }
        },
        FUZZY {
            @Override
            public Query getQuery(String field, Analyzer analyzer, String searchFor) {
                return new FuzzyQuery(new Term(field, searchFor));
            }
        };

        public abstract Query getQuery(String field, Analyzer analyzer, String searchFor);
    }

    private DocLanguage language;
    private boolean isDetailed;
    private boolean isColored;
    private LimitOfResults limitOfResults;
    private SearchType searchType;

    /**
     * Get the name of the field for the content for current language
     * @return string being description of the field
     */
    public String getContentField() {
        return this.language.getContentForLanguageFieldName();
    }

    /**
     *Get the name of te field for the file name for current language
     * @return string being description of te field
     */
    public String getFileNameField() {
        return this.language.getFileNameForLanguageFieldName();
    }

    /**
     * Get language {@link Analyzer} for current language
     * @return specified by settings {@link Analyzer}
     */
    public Analyzer getLanguageAnalyzer() {
        try {
            return new AnalyzerFactory().getForLanguage(this.language);
        } catch (UnknownLanguageException exc) {
            logger.error("Unknown language in settings!!! This should never happen. Setting Analyzer to EnglishAnalyzer for this search...");
            return new EnglishAnalyzer();
        }
    }

    /**
     * Check if short highlighting can be done based on the
     * type of current searching which is connected with
     * {@link com.procyk.maciej.index.IndexSearcher#MIN_LENGTH_OF_SHORT_HIGHLIGTH_QUERY}
     * @return true when short phrases can be highlighted otherwise false
     */
    public boolean canShortBeHighlighted() {
        return this.searchType != SearchType.FUZZY;
    }

    /**
     * Chech if setting has detailed description option on
     * @return true when on, false when off
     */
    public boolean isDetailed() {
        return isDetailed;
    }

    /**
     * Check if setting has colored description option on
     * @return true wen on, false when off
     */
    public boolean isColored() {
        return isColored;
    }

    /**
     * Get the current limit of results from settings
     * @return number of limit
     */
    public int getLimitOfResults() {
        return limitOfResults.getValue();
    }

    /**
     * Get the {@link Query} for the current settings type of the searching
     * @param field to be searched in by the query
     * @param analyzer to be used in query parsing
     * @param searchFor string to be searched
     * @return special query for current settings
     */
    public Query getQuery(String field, Analyzer analyzer, String searchFor) {
        return searchType.getQuery(field, analyzer, searchFor);
    }

    /**
     * Get the {@link Query} of content for current language settings
     * @param searchFor string to be searched
     * @return query for current settings
     */
    public Query getContentQuery(String searchFor) {
        return this.getQuery(this.getContentField(), this.getLanguageAnalyzer(), searchFor);
    }

    /**
     * Get the {@link Query} of file name for currrent language settings
     * @param searchFor string to be searched
     * @return query for current settings
     */
    public Query getFileNameQuery(String searchFor) {
        return this.getQuery(this.getFileNameField(), this.getLanguageAnalyzer(), searchFor);
    }

    /**
     * Set the language for current settings
     * @param language to be set
     */
    public void setLanguage(String language) {
        if (Objects.isNull(language))
            return;

        try {
            this.language = DocLanguage.valueOf(stringChanger.apply(language));
        } catch (IllegalArgumentException exc) {
            logger.warn("Bad search language specified: {}. Language is set to {}", language, this.language.toString());
        }
    }

    /**
     * Set if detailed option should be on/off
     * @param detailed option to be set
     */
    public void setDetailed(String detailed) {
        try {
            this.isDetailed = parseBooleanSetting(detailed);
        } catch (IllegalArgumentException exc) {
            logger.warn("Bad detailed option specified: {}. Detailed is set to {}", detailed, this.isDetailed);
        }
    }

    /**
     * Set if colored option should be on/off
     * @param colored option to be set
     */
    public void setColored(String colored) {
        try {
            this.isColored = parseBooleanSetting(colored);
        } catch (IllegalArgumentException exc) {
            logger.warn("Bad colored option specified: {}. Colored is set to {}", colored, this.isColored);
        }
    }

    /**
     * Set new limit of results for settings
     * @param limitOfResults new limit for search results
     */
    public void setLimitOfResults(String limitOfResults) {
        if (Objects.isNull(limitOfResults))
            return;

        try {
            int value = Integer.parseInt(limitOfResults);
            this.limitOfResults.setValue(value);
        } catch (NumberFormatException exc) {
            logger.warn("Bad limit value specified: {}. Limit is set to {}", limitOfResults, this.limitOfResults);
        }
    }

    /**
     * Set new search type for settings
     * @param searchType new search type from the {@link SearchType} names range
     */
    public void setSearchType(String searchType) {
        if (Objects.isNull(searchType))
            return;

        try {
            this.searchType = SearchType.valueOf(stringChanger.apply(searchType));
        } catch (IllegalArgumentException exc) {
            logger.warn("Bad search type specified: {}. Search type is set to {}", searchType, this.searchType.toString().toLowerCase());
        }
    }

    private SearchSettings() {
        this.setToDefault();
    }

    /**
     * Get the default version of settings with
     * colored on,
     * detailed on,
     * limit = 5,
     * search type = term,
     * search language = pl
     * @return default version of settings
     */
    public static SearchSettings getDefault() {
        return new SearchSettings();
    }

    private void setToAllOn() {
        this.language = DocLanguage.PL;
        this.isDetailed = true;
        this.isColored = true;
        this.limitOfResults = new LimitOfResults(5);
        this.searchType = SearchType.TERM;
    }

    private void setToDefault() {
        this.language = DocLanguage.EN;
        this.isDetailed = false;
        this.isColored = false;
        this.limitOfResults = new LimitOfResults();
        this.searchType = SearchType.TERM;
    }

    private boolean parseBooleanSetting(String setting) throws IllegalArgumentException {
        if (stringComparator.apply(ON, setting))
            return true;
        else if (stringComparator.apply(OFF, setting))
            return false;
        else
            throw new IllegalArgumentException();
    }

    @Override
    public String toString() {
        return "Current settings: language=" + language.toString() + ", details=" + (isDetailed ? ON : OFF) + ", color=" + (isColored ? ON : OFF) + ", limit=" + limitOfResults.getValue() + ", searchType=" + searchType;
    }
}
