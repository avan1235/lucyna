package com.procyk.maciej.properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Class for defining the limit of search results to easily manage
 * the changes of this setting
 */
public class LimitOfResults {

    private static Logger logger = LoggerFactory.getLogger(LimitOfResults.class);

    private static final int SPECIAL_ZERO_VALUE = Integer.MAX_VALUE;

    private int value;

    /**
     * Initialize new {@link LimitOfResults} with the {@link #SPECIAL_ZERO_VALUE}
     */
    public LimitOfResults() {
        this.value = SPECIAL_ZERO_VALUE;
    }

    public LimitOfResults(int value) {
        this.value = SPECIAL_ZERO_VALUE;
        this.setValue(value);
    }

    /**
     * Get the value of the limit
     * @return value of limit
     */
    public int getValue() {
        return value;
    }

    /**
     * Change the limit if can be changed to proper value. When given value
     * is not good then the value is not changed
     * @param value new value to be set to limit
     */
    public void setValue(int value) {
        if (value == 0)
            this.value = SPECIAL_ZERO_VALUE;
        else if (value > 0)
            this.value = value;
        else
            logger.warn("Limit of result not changed because {} is not good value to be set", value);
    }
}
