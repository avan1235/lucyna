package com.procyk.maciej.properties;

/**
 * Definitions of the constants connected with the usage of the
 * Lucene in the program and writing values to index and searching in
 * the indexed docs
 */
public class LuceneConstants {
    public static final String PATH = "path";

    public static final String CONTENT_PL = "content_pl";
    public static final String CONTENT_EN = "content_en";
    public static final String ALL_CONTENT = "all_content";

    public static final String FILE_NAME = "filename";
    public static final String FILE_NAME_PL = "filename_pl";
    public static final String FILE_NAME_EN = "filename_en";

    public static final String REGISTERED_PATH = "registered_path";
    public static final String ALL_SUB_DIR_AND_FILES_QUERY = "/*";
    public static final String ALL_QUERY = "*";
    public static final String EMPTY_QUERY = "";
}
