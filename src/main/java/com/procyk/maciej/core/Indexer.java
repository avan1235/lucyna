package com.procyk.maciej.core;

import com.procyk.maciej.input.IndexerCommandsExecutor;
import com.procyk.maciej.special.BadIndexerUsageException;
import com.procyk.maciej.special.ExitProgram;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Arrays;

/**
 * Indexer main class of the program that maintains the Indexer work and executes the tasks given
 * to the Indexer by the arguments. The possible arguments are
 * --purge : clean whole index
 * --add [path] : add single path to the indexed directories and index it recursively
 * --rm [path] : remove single path from indexed directories recursively. When the path was added using
 *               --add command then it is also removed from the saved directories. If not then
 *               it is only removed from indexed docs but can be reindexed
 * --reindex : index all the indexed paths recursively (so these which were added using the --add command)
 * --list : list indexed directories (so only these added to the index using --add command)
 */
public class Indexer {

    private static Logger logger = LoggerFactory.getLogger(Indexer.class);

    public static void main(String... args) {

        try {
            IndexerCommandsExecutor executor = new IndexerCommandsExecutor(args);
            executor.executeTask();
        } catch (BadIndexerUsageException exc) {
            logger.info("Bad arguments given for {}: {}", Indexer.class.getSimpleName(), Arrays.toString(args));
        }
        ExitProgram.exit();
    }
}
