package com.procyk.maciej.core;

import com.procyk.maciej.index.IndexCompleter;
import com.procyk.maciej.index.IndexSearcherReplAdapter;
import com.procyk.maciej.input.SearchEngineTerminal;
import com.procyk.maciej.output.OutputFormatter;
import com.procyk.maciej.output.SearchResultsFormatter;
import com.procyk.maciej.special.ExitProgram;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.jline.reader.Completer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Searcher for indexed files implementation with the specified commands enabled.
 * When using searcher and command not starts wit '%' then it is treated as the
 * searching phrase. Available in Searcher commands are:
 * %lang [en/pl] : change searching language so the language to analyze terms and
 *                 language of searching fields in documents
 * %details [on/off] : show details about search results
 * %color [on/off] : color the searched phrase in search results when details is on
 * %limit [int number] : limit the number of search results. When set to 0 the the limit is
 *                       Integer.MAX_VALUE
 */
public class Searcher {

    private static Logger logger = LoggerFactory.getLogger(Searcher.class);

    private static final String HELLO_COMPLETE_MESSAGE = "Use TAB key for commands completion";

    public static void main(String... args) {

        IndexSearcherReplAdapter executor = new IndexSearcherReplAdapter();
        OutputFormatter formatter = new SearchResultsFormatter();
        Completer completer = new IndexCompleter(executor.getSettings()); // give null to disable term completion

        try (SearchEngineTerminal terminal = new SearchEngineTerminal(executor, formatter, completer, HELLO_COMPLETE_MESSAGE)) {
            terminal.runRepl();
        } catch (Exception exc) {
            logger.error("Exception occurred in {} work. Exiting program...", Searcher.class.getSimpleName());
            logger.error("Execption message: {}", exc.getMessage());
            logger.error("Exception stack trace: {}", ExceptionUtils.getStackTrace(exc));
            ExitProgram.IOExit();
        }
        ExitProgram.exit();
    }
}
