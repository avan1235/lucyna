package com.procyk.maciej.special;

import java.io.PrintStream;

/**
 * Exception thrown when some error in document analysis occurred and
 * this document should be skipped in analysing process
 */
public class SkipException extends Exception {

    private final String messageWarning;

    public SkipException(String description) {
        super(description);
        messageWarning = description;
    }

    public void printWarning(PrintStream stream) {
        stream.println(this.getClass().getSimpleName() + " was thrown: " + this.messageWarning + " so this file will be skipped");
    }
}
