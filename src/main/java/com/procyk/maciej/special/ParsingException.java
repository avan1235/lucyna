package com.procyk.maciej.special;

/**
 * Exception thrown when parsing document error detected and parsed
 * file should be skipped in analysing
 */
public class ParsingException extends SkipException {

    public ParsingException(String description) {
        super(description);
    }
}
