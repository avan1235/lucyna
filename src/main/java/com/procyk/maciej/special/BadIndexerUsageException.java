package com.procyk.maciej.special;

/**
 * Exception thrown when bad usage of {@link com.procyk.maciej.core.Indexer} reported
 * for example because of bad command and the information could be than logged
 */
public class BadIndexerUsageException extends Exception {
}
