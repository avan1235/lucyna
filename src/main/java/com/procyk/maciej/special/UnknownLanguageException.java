package com.procyk.maciej.special;

/**
 * Special type of exception thrown when error occurred in detecting
 * the language of some input file
 */
public class UnknownLanguageException extends SkipException {

    public UnknownLanguageException(String description) {
        super(description);
    }
}
