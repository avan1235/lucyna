package com.procyk.maciej.special;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Exiting program executor to log the exiting with specified exit code
 * and easily control exiting from the application
 */
public class ExitProgram {

    private static Logger logger = LoggerFactory.getLogger(ExitProgram.class);

    private static final String EXIT_CODE_MESSAGE = "Exiting program with exit code {} ...";

    private enum ExitReason { NORMAL, STANDARD_ERROR, MEMORY }

    /**
     * Standard exit method to exit with exit code 0
     */
    public static void exit() {
        exit(ExitReason.NORMAL);
    }

    /**
     * Exit with the specified exit code and logs by the logger exiting event
     * @param exitCode of the program exiting process
     */
    private static void exit(ExitReason exitCode) {
        logger.info(EXIT_CODE_MESSAGE, exitCode);
        System.exit(exitCode.ordinal());
    }

    /**
     * Exit due to IOException so problem with memory
     */
    public static void IOExit() {
        exit(ExitReason.MEMORY);
    }

    /**
     * Exit due to other errors in program than memory problem
     */
    public static void errorExit() {
        exit(ExitReason.STANDARD_ERROR);
    }
}
