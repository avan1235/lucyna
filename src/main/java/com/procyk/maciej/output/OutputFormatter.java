package com.procyk.maciej.output;

import java.util.Collection;

/**
 * Formatter interface to connect lines of results into some characteristic
 * way and return single string as a result
 * Used in {@link com.procyk.maciej.input.IndexerCommandsExecutor} to format the
 * output from the responses of executor
 */
public interface OutputFormatter {
    /**
     * Create formatted version of the
     * @param lines to be formatted into single output
     * @return result formatted string
     */
    String getFormattedOutput(Collection<String> lines);
}
