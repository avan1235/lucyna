package com.procyk.maciej.output;

import org.apache.lucene.search.highlight.Formatter;
import org.apache.lucene.search.highlight.TokenGroup;
import org.jline.terminal.Terminal;
import org.jline.utils.AttributedStringBuilder;
import org.jline.utils.AttributedStyle;

import java.util.Objects;

/**
 * Context formatter for highlighting the search results phrases
 * in the given context for the search result
 */
public class ContextFormatter implements Formatter {

    private boolean highlightPhrases;
    private Terminal terminal;

    public ContextFormatter(boolean highlightPhrases, Terminal terminal) {
        this.highlightPhrases = highlightPhrases;
        this.terminal = terminal;
    }

    /**
     * Terminal to display the changed phrases in which should be
     * set to better terminals compatibility
     * @param terminal which is used in displaying
     */
    public void setTerminal(Terminal terminal) {
        this.terminal = terminal;
    }

    /**
     * Set if phrases sould be highlighted in the search results
     * @param highlightPhrases true when highlight otherwise false
     */
    public void setHighlightPhrases(boolean highlightPhrases) {
        this.highlightPhrases = highlightPhrases;
    }

    /**
     * Highlighting terms function which highlights terms with
     * score above zero if highlighting is enabled. Can be used with
     * tokenGroup equal to null in order to highlight some phrase
     * @param originalText in which phrase should be highlighted
     * @param tokenGroup contains one or several overlapping Tokens along with
     *                   their scores and positions
     * @return edited string
     */
    @Override
    public String highlightTerm(String originalText, TokenGroup tokenGroup) {
        if (Objects.nonNull(tokenGroup) && (tokenGroup.getTotalScore() <= 0 || !highlightPhrases))
            return originalText;

        AttributedStringBuilder builder = new AttributedStringBuilder();
        return builder.style(new AttributedStyle().backgroundOff()
                                                  .foreground(AttributedStyle.RED).bold())
                      .append(originalText)
                      .toAnsi(terminal);
    }
}
