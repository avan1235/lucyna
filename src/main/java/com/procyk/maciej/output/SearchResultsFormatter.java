package com.procyk.maciej.output;

import org.jline.utils.AttributedStringBuilder;
import org.jline.utils.AttributedStyle;

import java.util.Collection;
import java.util.Objects;

/**
 * {@link OutputFormatter} for printing the results of searcher
 * search results
 */
public class SearchResultsFormatter extends LineByLineOutputFormatter {

    private static final String FILE_COUNT_TITLE = "File count: ";

    @Override
    public String getFormattedOutput(Collection<String> lines) {
        if (Objects.isNull(lines))
            return "";

        AttributedStringBuilder builder = new AttributedStringBuilder();
        String result = builder.ansiAppend(FILE_COUNT_TITLE)
                               .style(new AttributedStyle().backgroundOff().bold())
                               .append(Integer.toString(lines.size()))
                               .style(new AttributedStyle().backgroundOff().boldOff())
                               .toAnsi(null);

        return result + System.lineSeparator() + super.getFormattedOutput(lines);
    }
}
