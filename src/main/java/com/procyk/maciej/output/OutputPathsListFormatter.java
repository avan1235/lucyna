package com.procyk.maciej.output;

import java.util.Collection;

/**
 * {@link OutputFormatter} for printing the results of indexer
 * indexed paths list
 */
public class OutputPathsListFormatter extends LineByLineOutputFormatter {

    /**
     * Get the list of paths in the index with the counted paths in the beginning
     * @param lines to be formatted into single output
     * @return formatted list of paths
     */
    @Override
    public String getFormattedOutput(Collection<String> lines) {

        return lines.size() + " path" + (lines.size() != 1 ? "s" : "") + " in index"
                + System.lineSeparator() + super.getFormattedOutput(lines);
    }
}
