package com.procyk.maciej.output;

import java.util.Collection;
import java.util.Objects;

/**
 * {@link OutputFormatter} which connects the given lines
 * separated by {@link System#lineSeparator()}
 */
public class LineByLineOutputFormatter implements OutputFormatter {

    /**
     * Get formatted output separated be new lines
     * @param lines to be formatted into single output
     * @return string with the formatted output
     */
    public String getFormattedOutput(Collection<String> lines) {
        StringBuilder builder = new StringBuilder();
        lines.stream().filter(Objects::nonNull).forEach(s -> appendWithNewLine(builder, s));
        return builder.toString();
    }

    private void appendWithNewLine(StringBuilder builder, String line) {
        builder.append(line);
        builder.append(System.lineSeparator());
    }
}
