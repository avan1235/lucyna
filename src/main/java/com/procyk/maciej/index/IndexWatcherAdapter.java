package com.procyk.maciej.index;

/**
 * Adapter class for the {@link IndexWriter} as {@link DirectoriesEventsHandler}
 * to handle some changes in filesystem and call specific {@link IndexWriter} functions
 */
public class IndexWatcherAdapter extends IndexWriter implements DirectoriesEventsHandler {

    /**
     * When new file created index as single doc
     * @param path to the new created file
     */
    @Override
    public void fileCreated(String path) {
        super.indexSingleDoc(path);
    }

    /**
     * When single file removed remove it from indexed paths as
     * @param path to the removed file. Remember that cannot check
     *             again in filesystem if for example file exists
     */
    @Override
    public void fileRemoved(String path) {
        super.removeSingleFile(path);
    }

    /**
     * When new directory created then index it recursively
     * @param path to the created directory
     */
    @Override
    public void directoryCreated(String path) {
        super.indexAddedDirectory(path);
    }

    /**
     * When some directory removed then remove indexed docs from directory recursively
     * @param path to the removed directory
     */
    @Override
    public void directoryRemoved(String path) {
        super.cleanDirectory(path);
    }
}
