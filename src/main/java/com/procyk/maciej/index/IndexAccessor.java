package com.procyk.maciej.index;

import com.procyk.maciej.properties.LuceneConstants;
import com.procyk.maciej.special.ExitProgram;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.Term;
import org.apache.lucene.search.*;
import org.apache.lucene.store.FSDirectory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;

import static com.procyk.maciej.properties.LuceneConstants.ALL_QUERY;
import static com.procyk.maciej.properties.LuceneConstants.REGISTERED_PATH;

/**
 * Class for maintaining the access to the index. Has common methods for {@link IndexWriter}
 * and {@link IndexSearcher}. Has to be remembered that th paths which are kept in the
 * documents of the specific files has to in linux format so with no '\\' chars (so there
 * is special method for converting this links) - caused by characteristic usage of
 * {@link WildcardQuery} which has symbol '\\' registered as special character which is
 * WILDCARD_ESCAPE {@value org.apache.lucene.search.WildcardQuery#WILDCARD_ESCAPE}
 */
public class IndexAccessor {

    private static Logger logger = LoggerFactory.getLogger(IndexAccessor.class);

    public static final Function<String, String> EDIT_BEFORE_INDEXING_FUNCTION = String::toLowerCase;

    /**
     * Name of the folder of index
     */
    private static final String INDEX_FOLDER = ".index";

    /**
     * Path to the index with indexed values
     */
    public static final String INDEX_PATH = System.getProperty("user.home") + File.separator + INDEX_FOLDER;

    private static final String WINDOWS_SEPARATOR = "\\";
    private static final String LINUX_SEPARATOR = "/";

    /**
     * Get the list of the indexed directories kept in index in the canonical form.
     * Returns only directories added using {@link com.procyk.maciej.index.IndexWriter#indexIndexedDirectory}
     * @return list of the indexed directories
     */
    public List<String> getIndexedDirectoriesCanonical() {
        ArrayList<String> resultNames = new ArrayList<>();
        try (IndexReader reader = DirectoryReader.open(FSDirectory.open(Paths.get(INDEX_PATH)))) {
            org.apache.lucene.search.IndexSearcher searcher = new org.apache.lucene.search.IndexSearcher(reader);
            Query query = new WildcardQuery(new Term(LuceneConstants.REGISTERED_PATH, ALL_QUERY));
            TopDocs results = searcher.search(query, Math.max(reader.numDocs(), 1));
            ScoreDoc[] hits = results.scoreDocs;

            for (ScoreDoc hit : hits)
                resultNames.add(searcher.doc(hit.doc).get(REGISTERED_PATH));

        } catch (IOException exc) {
            logger.error("Unable to read from index. Exiting...");
            ExitProgram.IOExit();
        }
        return resultNames;
    }

    /**
     * Converth the path in Windows format to linux format
     * @param old path to be converted
     * @return converted path in linux format
     */
    // package private access
    static String convertPathToLinuxFormat(String old) {
        if (File.separator.equals(WINDOWS_SEPARATOR))
            return old.replace(WINDOWS_SEPARATOR, LINUX_SEPARATOR);
        else
            return old;
    }
}
