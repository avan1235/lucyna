package com.procyk.maciej.index;

import com.procyk.maciej.output.ContextFormatter;
import com.procyk.maciej.properties.ReadOnlySearchSettings;
import com.procyk.maciej.properties.SearchSettings;
import com.procyk.maciej.special.ExitProgram;
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.document.Document;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.search.BooleanQuery;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TopDocs;
import org.apache.lucene.search.highlight.*;
import org.apache.lucene.store.FSDirectory;
import org.jline.terminal.Terminal;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.nio.file.Paths;
import java.util.*;

import static com.procyk.maciej.properties.LuceneConstants.*;

public class IndexSearcher extends IndexAccessor {

    private static Logger logger = LoggerFactory.getLogger(IndexSearcher.class);

    private static final int SHORT_FIND_STRING_LENGTH = 30;
    private static final int NUMBER_OF_LISTED_OCCURRENCES = 10;

    public static final int MIN_LENGTH_OF_SHORT_HIGHLIGTH_QUERY = 4;

    private static final int SHORT_STRING_MULTIPLY = 15;
    private static final int LONG_STRING_MULTIPLY = 5;

    private static final String OCCURRENCES_SEPARATOR = "...";

    private final SearchSettings settings;
    private final ContextFormatter formatter;

    public IndexSearcher() {
        this.settings = SearchSettings.getDefault();
        this.formatter = new ContextFormatter(this.settings.isColored(), null);
    }

    /**
     * Set terminal in which formatter of {@link IndexSearcher} will be used
     * @param terminal to be set for formatter
     */
    public void setTerminal(Terminal terminal) {
        this.formatter.setTerminal(terminal);
    }

    /**
     * Get the setting reference of {@link IndexSearcher}
     * @return reference to current search setting of searcher
     */
    public ReadOnlySearchSettings getSettings() {
        return new ReadOnlySearchSettings(this.settings);
    }

    /**
     * Search for specified string search and get the list of the results
     * with the characterized by settings style
     * @param toFind string to be searched for
     * @return list of search results
     */
    public List<SearchResult> searchFor(String toFind) {
        ArrayList<SearchResult> searchResults = new ArrayList<>();
        toFind = toFind.trim(); // trim for cutting unused spaces in searched string

        try (IndexReader reader = DirectoryReader.open(FSDirectory.open(Paths.get(INDEX_PATH)))) {
            org.apache.lucene.search.IndexSearcher searcher = new org.apache.lucene.search.IndexSearcher(reader);
            Query textQuery = this.settings.getContentQuery(EDIT_BEFORE_INDEXING_FUNCTION.apply(toFind));
            Query nameQuery = this.settings.getFileNameQuery(EDIT_BEFORE_INDEXING_FUNCTION.apply(toFind));

            TopDocs resultsText = searcher.search(textQuery, this.settings.getLimitOfResults());
            TopDocs resultsName = searcher.search(nameQuery, this.settings.getLimitOfResults());
            List<MergedSearchResult> mergedSearchResults = new ArrayList<>();

            this.mergeSearchResultsForContentAndNames(resultsText, resultsName, mergedSearchResults, this.settings.getLimitOfResults(), searcher);
            this.collectSearchResults(searchResults, searcher, textQuery, mergedSearchResults, toFind);

        } catch (IOException exc) {
            logger.error("Unable to read from index. Exiting...");
            ExitProgram.IOExit();
        }
        return searchResults;
    }

    /**
     * Collect search results to one list and edit them in the specified format
     * @param searchResults collection to which items will be added
     * @param searcher current searcher for index
     * @param query current searching query
     * @param results list of merged results
     * @param toFind phrase that we look for in current search
     * @throws IOException when problem with accessing index occurred
     */
    @SuppressWarnings("deprecation")
    private void collectSearchResults(Collection<SearchResult> searchResults, org.apache.lucene.search.IndexSearcher searcher, Query query, List<MergedSearchResult> results, String toFind) throws IOException {

        if (this.settings.isDetailed() && (toFind.length() > MIN_LENGTH_OF_SHORT_HIGHLIGTH_QUERY || this.settings.canShortBeHighlighted())) {
            // firstly actualize settings connected with the coloring details in searched phrases
            this.formatter.setHighlightPhrases(this.settings.isColored());

            QueryScorer scorer = new QueryScorer(query);
            Highlighter highlighter = new Highlighter(this.formatter, scorer);
            Fragmenter fragmenter = new SimpleSpanFragmenter(scorer, getFragmentSize(toFind));
            highlighter.setTextFragmenter(fragmenter);

            for (MergedSearchResult hit : results) {
                int docId = hit.docId;
                String field = hit.detailedFieldName;
                Document doc = searcher.doc(docId);
                TokenStream stream = TokenSources.getAnyTokenStream(searcher.getIndexReader(), docId, field, this.settings.getLanguageAnalyzer());
                String text = doc.get(field);
                String[] frags;
                try {
                    frags = highlighter.getBestFragments(stream, text, NUMBER_OF_LISTED_OCCURRENCES);
                } catch (Exception exc) {
                    logger.warn("Error when highlighting searched phrase {}. As the context of found text will be returned whole document", toFind);
                    frags = new String[1];
                    frags[0] = doc.get(field);
                }
                StringBuilder resultContext = new StringBuilder();
                Arrays.stream(frags).forEach(s -> appendContextPart(resultContext, s));
                searchResults.add(new SearchResult(doc.get(PATH), ALL_CONTENT.equals(field) ?
                                                                  formatResultContext(resultContext.toString()) :
                                                                  this.formatter.highlightTerm(doc.get(FILE_NAME), null)));
            }
        } else {
            for (MergedSearchResult hit : results)
                searchResults.add(new SearchResult(searcher.doc(hit.docId).get(PATH)));
        }
    }

    /**
     * Merge results from searching in filenames and content of files
     * @param resultText search results from searching in texts of docs
     * @param resultNames search results from searching in names of docs
     * @param mergedSearchResults collection to which result will be added
     * @param limit limit of added results
     * @param searcher used in searching for docs in index process
     */
    private void mergeSearchResultsForContentAndNames(TopDocs resultText, TopDocs resultNames, Collection<? super MergedSearchResult> mergedSearchResults, int limit, org.apache.lucene.search.IndexSearcher searcher) {
        ScoreDoc[] text = resultText.scoreDocs;
        ScoreDoc[] name = resultNames.scoreDocs;

        Set<String> mergedDocsPaths = new HashSet<>();

        logger.info("Start merging two sets of search results");
        int iText = 0, iName = 0, iLimit = 0;
        while (iLimit < limit && (iText < text.length && iName < name.length)) {
            String textPath = getPathOfDoc(text[iText].doc, searcher);
            String namePath = getPathOfDoc(name[iName].doc, searcher);
            if (text[iText].score > name[iName].score) {
                addSingleDocIfNotAlready(text[iText].doc, textPath, ALL_CONTENT, mergedSearchResults, mergedDocsPaths);
                iText++;
            } else  {
                addSingleDocIfNotAlready(name[iName].doc, namePath, FILE_NAME, mergedSearchResults, mergedDocsPaths);
                iName++;
            }
            iLimit++;
        }
        iLimit = addRestWhileMerging(iText, text, iLimit, limit, ALL_CONTENT, mergedSearchResults, mergedDocsPaths, searcher);
        addRestWhileMerging(iName, name, iLimit, limit, FILE_NAME, mergedSearchResults, mergedDocsPaths, searcher);

        logger.info("Merged two sets of search results of size {} and {}", text.length, name.length);
    }

    private int addRestWhileMerging(int iRest, ScoreDoc[] rest, int iLimit, int limit, String field, Collection<? super MergedSearchResult> mergedSearchResults, Set<String> mergedDocsPaths, org.apache.lucene.search.IndexSearcher searcher) {
        while (iLimit < limit && iRest < rest.length) {
            String namePath = getPathOfDoc(rest[iRest].doc, searcher);
            addSingleDocIfNotAlready(rest[iRest].doc, namePath, field, mergedSearchResults, mergedDocsPaths);
            iRest++;
            iLimit++;
        }
        return iLimit;
    }

    private void addSingleDocIfNotAlready(int id, String path, String field, Collection<? super MergedSearchResult> mergedSearchResults, Set<String> mergedDocsPaths) {
        if (!mergedDocsPaths.contains(path)) {
            mergedDocsPaths.add(path);
            mergedSearchResults.add(new MergedSearchResult(id, field));
        }
    }

    private String getPathOfDoc(int docId, org.apache.lucene.search.IndexSearcher searcher) {
        try {
            return searcher.doc(docId).get(PATH);
        } catch (IOException exc) {
            logger.warn("Error while searching. Returns empty string as the path of the current file");
            return "";
        }
    }

    private String formatResultContext(String context) {
        if (context.lastIndexOf(OCCURRENCES_SEPARATOR) >= 0) {
            return context.substring(0, context.lastIndexOf(OCCURRENCES_SEPARATOR));
        } else {
            return context;
        }
    }

    private int getFragmentSize(String toFind) {
        if (toFind.length() <= SHORT_FIND_STRING_LENGTH)
            return toFind.length() * SHORT_STRING_MULTIPLY;
        else
            return toFind.length() * LONG_STRING_MULTIPLY;
    }

    /**
     * Set language of searching
     * @param language to be set
     */
    public void setLanguage(String language) {
        this.settings.setLanguage(language);
    }

    /**
     * Set detailed option for search results showing
     * @param detailed to be set
     */
    public void setDetailed(String detailed) {
        this.settings.setDetailed(detailed);
    }

    /**
     * Set limit of search results
     * @param limit to be set
     */
    public void setLimit(String limit) {
        this.settings.setLimitOfResults(limit);
    }

    /**
     * Set colored option for search results showing
     * @param colored to be set
     */
    public void setColored(String colored) {
        this.settings.setColored(colored);
    }

    /**
     * Set search type for searching
     * @param searchType to be set
     */
    public void setSearchType(String searchType) {
        this.settings.setSearchType(searchType);
    }

    /**
     * Inner class for easier representation of merging results from searching
     * in content of files and in file names. Consists of the search result docId
     * and the field name in which this doc was found for search phrase
     */
    private class MergedSearchResult {
        private int docId;
        private String detailedFieldName;

        private MergedSearchResult(int docId, String detailedFieldName) {
            this.docId = docId;
            this.detailedFieldName = detailedFieldName;
        }

        @Override
        public String toString() {
            return this.getClass().getSimpleName() + " docId=" + this.docId + ", detailedFieldName=" + this.detailedFieldName;
        }
    }

    private void appendContextPart(StringBuilder builder, String s) {
        builder.append(s).append(OCCURRENCES_SEPARATOR);
    }
}