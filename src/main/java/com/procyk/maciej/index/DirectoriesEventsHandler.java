package com.procyk.maciej.index;

/**
 * Interface for handling different events when some changes
 * detected by {@link DirectoriesWatcher}
 */
public interface DirectoriesEventsHandler {

    /**
     * Method called when file was created
     * @param path to the new created file
     */
    void fileCreated(String path);

    /**
     * Method called when file was removed
     * @param path to the removed file. Remember that cannot check
     *             again in filesystem if for example file exists
     *             or was a regular file
     */
    void fileRemoved(String path);

    /**
     * Method called when file changed. Default action is to
     * treat modification as the deletion and creation of the
     * new file with the same path
     * @param path to the modified file
     */
    default void fileModified(String path) {
        fileRemoved(path);
        fileCreated(path);
    }

    /**
     * Method called when new directory was created
     * @param path to the created directory
     */
    void directoryCreated(String path);

    /**
     * Method called when directory was removed
     * @param path to the removed directory
     */
    void directoryRemoved(String path);
}
