package com.procyk.maciej.index;

import com.procyk.maciej.special.ExitProgram;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.*;

import static java.nio.file.LinkOption.NOFOLLOW_LINKS;
import static java.nio.file.StandardWatchEventKinds.*;

/**
 * Recursive directories watcher implementation which uses
 * {@link DirectoriesEventsHandler} to handle the actions
 * when some change to filesystem was noticed
 */

public class DirectoriesWatcher {

	private static Logger logger = LoggerFactory.getLogger(DirectoriesWatcher.class);
	
	private WatchService watcher;
	private Map<WatchKey, Path> keys;
	private Set<Path> registeredDirectories;
	private DirectoriesEventsHandler eventsHandler;
	private boolean trace = false;

	@SuppressWarnings("unchecked")
	private static <T> WatchEvent<T> castToWatchEvent(WatchEvent<?> event) {
		return (WatchEvent<T>) event;
	}

	/**
	 * Register the given directory with the WatchService as a single directory
	 * and put the directory to the set of already registered directories
	 * to have the information in the future if the removed path was a file or directory
	 */
	private void registerNewDirectory(Path dir) throws IOException {
		WatchKey key = dir.register(watcher, ENTRY_CREATE, ENTRY_DELETE, ENTRY_MODIFY);
		if (trace) {
			Path prev = keys.get(key);
			if (prev == null) {
				logger.info("RegisterNewDirectory: {}", dir);
			}
			else if (!dir.equals(prev)) {
			    logger.info("Update: {} -> {}", prev, dir);
			}
		}
		keys.put(key, dir);
		registeredDirectories.add(dir);
	}

	/**
	 * Register the given directory, and all its sub-directories, with the
	 * WatchService and put them to the Set of the registered ones.
	 * @param start the path to start recursively adding directories
	 * @throws IOException when visiting directory throws exception
	 */
	private void registerAll(final Path start) throws IOException {
		// registerNewDirectory directory and sub-directories
		Files.walkFileTree(start, new SimpleFileVisitor<Path>() {
			@Override
			public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs) throws IOException {
				registerNewDirectory(dir);
				return FileVisitResult.CONTINUE;
			}
		});
	}

	/**
	 * Creates a WatchService and registers the given collection of directories
	 * @param directories collection of directories to index recursively
	 * @param handler for handling the changes to filesystem when changed
	 */
	public DirectoriesWatcher(Collection<String> directories, DirectoriesEventsHandler handler) {
		try {
		    this.eventsHandler = handler;
            this.keys = new HashMap<>();
            this.registeredDirectories = new HashSet<>();
            this.watcher = FileSystems.getDefault().newWatchService();

            for (String dir : directories) {
                Path path = Paths.get(dir);
                logger.info("Adding {} and all subdirectories to the watched directories...", dir);
                registerAll(path);
                logger.info("Done adding directories.");

            }
        } catch (IOException exc) {
		    logger.error("IO error: {}", exc.getMessage());
            ExitProgram.IOExit();
        }

		this.trace = true;
	}

	public void processEvents() {
	    logger.info("Starting watching registered directories...");
		while (true) {

			// wait for key to be signalled
			WatchKey key;
			try {
				key = watcher.take();
			} catch (InterruptedException exc) {
				return;
			}

			Path dir = keys.get(key);
			if (dir == null) {
				logger.warn("WatchKey not recognized.");
				continue;
			}

			for (WatchEvent<?> event : key.pollEvents()) {
				WatchEvent.Kind<?> kind = event.kind();

				if (kind == OVERFLOW)
					continue;

				WatchEvent<Path> ev = castToWatchEvent(event);
				Path name = ev.context();
				Path changedPath = dir.resolve(name);

				logger.info("{}: {}", event.kind().name(), changedPath);

				try {
                    if (kind == ENTRY_CREATE) {
                        if (Files.isDirectory(changedPath, NOFOLLOW_LINKS)) {
                            eventsHandler.directoryCreated(changedPath.toString());
                            registerAll(changedPath);
                        }
                        else if (Files.isRegularFile(changedPath, NOFOLLOW_LINKS)) {
                            eventsHandler.fileCreated(changedPath.toString());
                        }
                    }
                    else if (kind == ENTRY_MODIFY && Files.isRegularFile(changedPath, NOFOLLOW_LINKS)) {
                        eventsHandler.fileModified(changedPath.toString());
                    }
                    else if (kind == ENTRY_DELETE) {
                        if (isDirectory(changedPath)) {
                            eventsHandler.directoryRemoved(changedPath.toString());
                            registeredDirectories.remove(changedPath);
                        }
                        else {
                            eventsHandler.fileRemoved(changedPath.toString());
                        }
                    }
                } catch (IOException exc) {
					logger.warn("Error when registering the directories in {}. Smoe directories may not be watched.", changedPath.toString());
					continue;
				}
			}

			// reset key and remove from set if directory no longer accessible
			boolean valid = key.reset();
			if (!valid) {
				keys.remove(key);

				if (keys.isEmpty()) {
					break;
				}
			}
		}
	}

	/**
	 * Chech if given path is a directory by checking in already added paths
	 * @param path to be checked
	 * @return true when path is directory, otherwise false
	 */
	private boolean isDirectory(Path path) {
		return registeredDirectories.contains(path);
	}
}
