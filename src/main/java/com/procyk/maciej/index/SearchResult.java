package com.procyk.maciej.index;

import org.jline.utils.AttributedStringBuilder;
import org.jline.utils.AttributedStyle;

import java.util.Objects;

/**
 * Search result class which has the option of special formatting
 * the output result to the string format and when printing
 * user don't have to worry about that if the search result has some context
 */
public class SearchResult {
    private String path;
    private String context;

    public SearchResult(String path, String context) {
        this.path = path;
        this.context = context;
    }

    public SearchResult(String path) {
        this.path = path;
        this.context = null;
    }

    /**
     * Function which returns the{@link SearchResult} in the characteristic format
     * @return formatted string
     */
    @Override
    public String toString() {
        AttributedStringBuilder builder = new AttributedStringBuilder();

        String result = builder.style(new AttributedStyle()
                               .backgroundOff().bold())
                               .ansiAppend(this.path)
                               .toAnsi(null);

        if (Objects.nonNull(this.context) && this.context.length() > 0)
            result = result + ":" + System.lineSeparator() + context;
        return result;
    }
}
