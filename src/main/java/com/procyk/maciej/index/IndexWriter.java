package com.procyk.maciej.index;

import com.procyk.maciej.docsmanagers.DocExtractor;
import com.procyk.maciej.docsmanagers.LanguageDetector;
import com.procyk.maciej.docsmanagers.PlEnAnalyzerWrapper;
import com.procyk.maciej.properties.DocLanguage;
import static com.procyk.maciej.properties.LuceneConstants.*;
import static java.nio.file.LinkOption.NOFOLLOW_LINKS;

import com.procyk.maciej.special.ExitProgram;
import com.procyk.maciej.special.ParsingException;
import com.procyk.maciej.special.SkipException;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.StringField;
import org.apache.lucene.document.TextField;
import org.apache.lucene.index.*;
import org.apache.lucene.search.*;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.List;
import java.util.Objects;

/**
 * Index creating and actualizing class for management of
 * indexed documents
 */
public class IndexWriter extends IndexAccessor {

    private static Logger logger = LoggerFactory.getLogger(IndexWriter.class);

    // static initialize the folder where the index will be saved
    // when initialization fails exits the program
    static {
        Path path = Paths.get(INDEX_PATH);
        if (!Files.exists(path)) {
            try {
                Files.createDirectory(path);
            } catch (IOException exc) {
                logger.error("Path {} couldn't be created. Exiting the program...", path.toString());
                ExitProgram.IOExit();
            }
        }
    }

    /**
     * Remove all registered paths from the index and the documents indexed under these paths
     * and then index again all the path which where saved under REGISTERED_PATH term in the index
     */
    public void reindexAll() {
        List<String> indexedPaths = getIndexedDirectoriesCanonical();
        cleanIndex();
        for (String path : indexedPaths)
            indexIndexedDirectory(path);
    }

    /**
     * Removes from index all documents so the indexed one and also the
     * documents which keep the registered paths as memory manager
     */
    public void cleanIndex() {
        try (Directory dir = FSDirectory.open(Paths.get(INDEX_PATH));
             org.apache.lucene.index.IndexWriter writer = new org.apache.lucene.index.IndexWriter(dir, new IndexWriterConfig())) { // OTDO test
            writer.deleteDocuments(new MatchAllDocsQuery());
        } catch (IOException exc) {
            logger.info("Reading index error. Index cannot be cleaned. Exiting...");
            ExitProgram.IOExit();
        }
    }

    /**
     * Remove from the indexed docs single file
     * @param path to the file to be removed
     */
    public void removeSingleFile(String path) {
        clean(path, false, false);
    }

    /**
     * Add single document to the index
     * @param path to the added document
     */
    public void indexSingleDoc(String path) {
        if (Objects.isNull(path))
            return;

        Path filePath = Paths.get(path);
        if (!Files.exists(filePath) || !Files.isRegularFile(filePath, NOFOLLOW_LINKS) || !Files.isReadable(filePath))
            return;


        try (Directory dir = FSDirectory.open(Paths.get(INDEX_PATH))) {
            Analyzer analyzer = new PlEnAnalyzerWrapper();
            IndexWriterConfig iwc = new IndexWriterConfig(analyzer);
            iwc.setOpenMode(IndexWriterConfig.OpenMode.CREATE_OR_APPEND);
            try (org.apache.lucene.index.IndexWriter writer = new org.apache.lucene.index.IndexWriter(dir, iwc)) {
                indexDoc(writer, filePath);
            }
        } catch (IOException exc) {
            logger.info("Index couldn't be opened. Fatal error. Exiting program...");
            ExitProgram.IOExit();
        } catch (SkipException exc) {
            logger.info("File {} couldn't be indexed so will be skipped", path);
        }
    }

    /**
     * Cleans the given directory from the index, so removes from index
     * all files that are indexed under the path "path/*" so every subdirectory
     * is also removed. When the given path is the registered then it is removed
     * from the index REGISTERED_PATHs
     * @param path - path to be removed from index
     */
    public void cleanDirectory(String path) {
        clean(path, true, true);
    }

    /**
     * Remove all the documents indexed under the given directory from the index
     * so removes from index all files that are indexed under the path "path/*"
     * so every subdirectory is also removed.
     * @param path - path to directory to be removed
     */
    public void removeDirectory(String path) {
        clean(path, false, true);
    }

    /**
     * Index new directory and all its subdirectories and files
     * but don't add this directory to the indexed directories list
     * @param path - path to directory to be indexed
     */
    public void indexAddedDirectory(String path) {
        indexDirectory(path, false);
    }

    /**
     * Index new directory with all files and subdirectories and
     * add it to the indexed directories in the index file
     * @param path - path to directory to be added
     */
    public void indexIndexedDirectory(String path) {
        indexDirectory(path, true);
    }

    /**
     * Index specified directory so all the files in directory and all subdirectories
     * @param path to the directory to be indexed
     * @param createNewIndex true when add directory to list of indexed directories
     *                       otherwise false
     */
    private void indexDirectory(String path, boolean createNewIndex) {
        if (Objects.isNull(path) || !Files.exists(Paths.get(path)))
            return;

        final Path docDir = Paths.get(path);
        if (!Files.isReadable(docDir)) {
            logger.info("Directory not readable {} so won't be indexed", docDir);
            return;
        }

        try (Directory dir = FSDirectory.open(Paths.get(INDEX_PATH));
             org.apache.lucene.index.IndexWriter writer = new org.apache.lucene.index.IndexWriter(dir, new IndexWriterConfig(new PlEnAnalyzerWrapper()))) {

            writeDirectoryToIndex(writer, docDir);
            indexDocsRecursively(writer, docDir);

        } catch (SkipException exc) {
            logger.info("Directory {} cannot be written to index. Skipping indexing", docDir);
        }
        catch (IOException exc) {
            logger.info("Index couldn't be opened. Fatal error. Exiting program...");
            ExitProgram.IOExit();
        }
    }

    /**
     * Save single document to the index
     * @param writer to the index to access index
     * @param path to the indexed directory
     * @throws SkipException when cannot open given path so should be skipped in indexing
     */
    private void writeDirectoryToIndex(final org.apache.lucene.index.IndexWriter writer, Path path) throws SkipException {
        Document doc = new Document();
        try {
            String addedPath = new File(path.toString()).getCanonicalPath();
            doc.add(new StringField(REGISTERED_PATH, addedPath, Field.Store.YES));
            writer.deleteDocuments(new Term(REGISTERED_PATH, addedPath));
            writer.addDocument(doc);
            writer.commit();
        } catch (IOException exc) {
            throw new SkipException("Permission to directory denied. Directory " + path.toString() + " wil be skipped");
        }
    }

    /**
     * Index doc in a recursive way by traversing through directories with
     * {@link java.nio.file.LinkOption#NOFOLLOW_LINKS} and indexing only
     * regular files
     * @param writer to index found docs
     * @param path to start recursive searching for docs and indexing
     */
    private void indexDocsRecursively(final org.apache.lucene.index.IndexWriter writer, Path path) {
        try {
            if (Files.isDirectory(path, NOFOLLOW_LINKS)) {
                Files.walkFileTree(path, new SimpleFileVisitor<Path>() {
                    @Override
                    public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) {
                        try {
                            if (attrs.isRegularFile())
                                indexDoc(writer, file);
                        } catch (SkipException exc) {
                            logger.info("Unsupported file type of {}. Won't be indexed", file);
                        }
                        return FileVisitResult.CONTINUE;
                    }
                });
            } else {
                if (Files.isRegularFile(path, LinkOption.NOFOLLOW_LINKS))
                    indexDoc(writer, path);
            }
        } catch (SkipException | IOException exc) {
            logger.info("Permission to directory {} denied. Won't be indexed.", path);
        }
    }

    /**
     * Index document by creating {@link Document} of lucene index and writing it to
     * the index using current writer
     * @param writer to be used to edit index
     * @param file to be indexed as the new document
     * @throws SkipException when error in creating document occurred
     */
    private void indexDoc(final org.apache.lucene.index.IndexWriter writer, Path file) throws SkipException {
        Document doc = createStandardDocument(file);

        try {
            if (writer.getConfig().getOpenMode() == IndexWriterConfig.OpenMode.CREATE)
                writer.addDocument(doc);
            else
                writer.updateDocument(new Term(PATH, convertPathToLinuxFormat(new File(file.toString()).getCanonicalPath())), doc);

        } catch (IllegalArgumentException exc) {
            logger.info("Document {} cannot be indexed because of it's structure", file.toString());
        } catch (IOException exc) {
            logger.info("Error reading to memory when indexing document {}", file.toString());
            ExitProgram.IOExit();
        }
    }

    /**
     * Clean from index file the written files or all directories.
     * @param path - string to the being removed resource
     * @param cleanFromRegisteredPath - set to true if path should also be removed from indexed directories
     * @param isDirectory - set to true if directory has to be removed (so al subdirs and files)
     */
    private void clean(String path, boolean cleanFromRegisteredPath, boolean isDirectory) {
        if (Objects.isNull(path))
            return;

        try (Directory dir = FSDirectory.open(Paths.get(INDEX_PATH))) {
            String addedPath = new File(path).getCanonicalPath();
            try (org.apache.lucene.index.IndexWriter writer = new org.apache.lucene.index.IndexWriter(dir, new IndexWriterConfig())) {
                String changedPath = convertPathToLinuxFormat(addedPath) + (isDirectory ? ALL_SUB_DIR_AND_FILES_QUERY : EMPTY_QUERY);
                writer.deleteDocuments(new WildcardQuery(new Term(PATH, changedPath)));

                if (cleanFromRegisteredPath)
                    writer.deleteDocuments(new Term(REGISTERED_PATH, addedPath));
            }
        } catch (IOException exc) {
            logger.info("Reading index error. Directory {} cannot be removed from index. Exiting...", path);
            ExitProgram.IOExit();
        }
    }

    /**
     * Create new document for the indexing process
     * @param filePath of the file to be indexed
     * @return new created document
     * @throws SkipException when error in accessing file occurred
     */
    private Document createStandardDocument(Path filePath) throws SkipException {
        Document doc = new Document();

        try (InputStream stream = Files.newInputStream(filePath)) {
            File inputFile = new File(filePath.toString());

            String fileName = inputFile.getCanonicalFile().getName();
            doc.add(new StringField(PATH, convertPathToLinuxFormat(inputFile.getCanonicalPath()), Field.Store.YES));
            doc.add(new StringField(FILE_NAME, fileName, Field.Store.YES));

            String content = DocExtractor.parseToString(stream);
            DocLanguage lang = LanguageDetector.identifyLanguage(content);

            doc.add(new StringField(ALL_CONTENT, content.replaceAll("\\r\\n\\r\\n|\\r\\r|\\n\\n", "\n").replaceAll("[\\n]+", "\n"), Field.Store.YES));

            String contentType = lang == DocLanguage.PL ? CONTENT_PL : CONTENT_EN;
            doc.add(new TextField(contentType, new StringReader(EDIT_BEFORE_INDEXING_FUNCTION.apply(content))));

            String nameType = lang == DocLanguage.PL ? FILE_NAME_PL : FILE_NAME_EN;

            String[] nameSplited = fileName.split("[\\s]+|[.]+|[-]+");

            for (String namePart : nameSplited) {
                doc.add(new StringField(nameType, EDIT_BEFORE_INDEXING_FUNCTION.apply(namePart), Field.Store.YES));
                doc.add(new StringField(nameType, namePart, Field.Store.YES));
            }
            doc.add(new StringField(nameType, EDIT_BEFORE_INDEXING_FUNCTION.apply(fileName), Field.Store.YES));
            doc.add(new StringField(nameType, fileName, Field.Store.YES));

            logger.info("Successfully created doc for the file {}", fileName);
        } catch (ParsingException exc) {
            logger.info("File {} has bad (not recognizable language) or no input inside so will not be indexed", filePath);
        } catch (IOException exc) {
            throw new SkipException(exc.getMessage());
        }
        return doc;
    }
}
