package com.procyk.maciej.index;

import com.procyk.maciej.input.ReplExecutor;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.jline.terminal.Terminal;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collection;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * Adapter for the {@link IndexSearcher} as the {@link ReplExecutor} in
 * search engine terminal
 */
public class IndexSearcherReplAdapter extends IndexSearcher implements ReplExecutor {

    private static final Logger logger = LoggerFactory.getLogger(IndexSearcherReplAdapter.class);

    private static final String CONTROL_SEQUENCE = "%";
    private static final String SPACE = " ";

    private static final String LANG_SETTING = "lang";
    private static final String DETAIL_SETTING = "details";
    private static final String COLOR_SETTING = "color";
    private static final String LIMIT_SETTING = "limit";

    public IndexSearcherReplAdapter() {
        System.out.println(this.getSettings().toString());
    }

    /**
     * Execute the tasks specified for the Searcher. When searching and get no
     * search results then returns empty list. When some setting called the null
     * is being returned
     * @param command to be parsed by executor
     * @return collection of results of search if any, for settings null
     */
    @Override
    public Collection<String> execute(String command) {
        try {
            if (Objects.nonNull(command)) {
                if (command.startsWith(CONTROL_SEQUENCE)) {
                    String control = command.substring(command.indexOf(CONTROL_SEQUENCE) + CONTROL_SEQUENCE.length());

                    if (control.contains(SPACE)) {
                        String settingCommand = control.substring(0, control.indexOf(SPACE));
                        String setting = control.substring(control.indexOf(SPACE) + SPACE.length());

                        switch (settingCommand) {
                            case LANG_SETTING:
                                this.setLanguage(setting);
                                break;
                            case DETAIL_SETTING:
                                this.setDetailed(setting);
                                break;
                            case COLOR_SETTING:
                                this.setColored(setting);
                                break;
                            case LIMIT_SETTING:
                                this.setLimit(setting);
                                break;
                        }
                    } else {
                        this.setSearchType(control);
                    }

                } else {
                    return this.searchFor(command).stream().map(SearchResult::toString).collect(Collectors.toList());
                }
            }
        } catch (IndexOutOfBoundsException exc) {
            logger.info("Exception message: {}", ExceptionUtils.getRootCause(exc).getMessage());
            logger.info("Bad input command: {}", command);
        }
        return null;
    }

    /**
     * Set the terminal used by formatter of search results
     * @param terminal which is used in repl mode
     */
    @Override
    public void setTerminal(Terminal terminal) {
        super.setTerminal(terminal);
    }
}
