package com.procyk.maciej.index;

import com.procyk.maciej.properties.ReadOnlySearchSettings;
import com.procyk.maciej.special.ExitProgram;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.search.spell.Dictionary;
import org.apache.lucene.search.spell.LuceneDictionary;
import org.apache.lucene.search.suggest.Lookup;
import org.apache.lucene.search.suggest.analyzing.AnalyzingSuggester;
import org.apache.lucene.store.FSDirectory;
import org.jline.reader.Candidate;
import org.jline.reader.Completer;
import org.jline.reader.LineReader;
import org.jline.reader.ParsedLine;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.nio.file.Paths;
import java.util.List;

/**
 * Implementation of the Completer interface for the auto completion of words in search index.
 * Completion based on the set of tokens which start with currently inputted prefix of word.
 * Every time the complete method is called, the index is opened and read tokens from
 * the current language field from {@link com.procyk.maciej.properties.LuceneConstants} using
 * the proper language analyzer {@link org.apache.lucene.analysis.Analyzer}
 */
public class IndexCompleter implements Completer {

    private static final Logger logger = LoggerFactory.getLogger(IndexCompleter.class);

    /**
     * Max number of lookup results taken from current tokens dictionary
     */
    private static final int NUM_OF_LOOKUP_RESULTS = 10;

    private final ReadOnlySearchSettings searchSettings;

    /**
     * Create new index based completer based on setting of current {@link IndexSearcher}
     * @param currentSearchSettings     current settings reference from IndexSearcher
     */
    public IndexCompleter(ReadOnlySearchSettings currentSearchSettings) {
        this.searchSettings = currentSearchSettings;
    }

    /**
     * Opens the dictionary based on the tokens from specified by settings language field
     * and writes to the candidates list as only key from the tokens
     * @param reader current line reader
     * @param commandLine already inputted line
     * @param candidates list of candidate to add candidate to
     */
    @Override
    public void complete(LineReader reader, ParsedLine commandLine, List<Candidate> candidates) {
        assert commandLine != null;
        assert candidates != null;

        String buffer = commandLine.word().substring(0, Math.max(commandLine.wordCursor(), 0));
        buffer = IndexAccessor.EDIT_BEFORE_INDEXING_FUNCTION.apply(buffer);

        this.addCandidatesOfField(candidates, buffer, this.searchSettings.getContentField());
        this.addCandidatesOfField(candidates, buffer, this.searchSettings.getFileNameField());
    }

    /**
     * Add candidates from {@link LuceneDictionary} build upon current index
     * @param candidates list to which candidates will be added
     * @param buffer current line buffer to search for candidates
     * @param field in whic search for candidates
     */
    private void addCandidatesOfField(List<Candidate> candidates, String buffer, String field) {
        try (DirectoryReader indexReader = DirectoryReader.open(FSDirectory.open(Paths.get(IndexAccessor.INDEX_PATH)))) {
            AnalyzingSuggester suggester = new AnalyzingSuggester(indexReader.directory(), "", searchSettings.getLanguageAnalyzer());
            Dictionary dictionary = new LuceneDictionary(indexReader, field);
            suggester.build(dictionary);
            List<Lookup.LookupResult> results = suggester.lookup(buffer, false, NUM_OF_LOOKUP_RESULTS);
            results.stream().map(this::lookupToStringConvert).map(Candidate::new).forEach(candidates::add);
        } catch (IOException exc) {
            logger.error("Unable to read from index. Exiting...");
            ExitProgram.IOExit();
        }
    }

    /**
     * Conversion from {@link Lookup.LookupResult} to {@link String} for lambda conversion
     * @param lookupResult to be converted to String
     * @return converted string of {@link Lookup.LookupResult} content
     */
    private String lookupToStringConvert(Lookup.LookupResult lookupResult) {
        return lookupResult.key.toString();
    }
}
