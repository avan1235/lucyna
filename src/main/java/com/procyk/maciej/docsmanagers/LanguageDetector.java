package com.procyk.maciej.docsmanagers;

import com.procyk.maciej.properties.DocLanguage;
import com.procyk.maciej.special.ExitProgram;
import com.procyk.maciej.special.ParsingException;
import org.apache.tika.langdetect.OptimaizeLangDetector;
import org.apache.tika.language.detect.LanguageConfidence;
import org.apache.tika.language.detect.LanguageResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

/**
 * Static methods based class which gives the ability to detect the language
 * of the given part of some text.
 */
public class LanguageDetector {

    private static final Logger logger = LoggerFactory.getLogger(LanguageDetector.class);

    private static final org.apache.tika.language.detect.LanguageDetector detector = new OptimaizeLangDetector();

    static {
        try {
            detector.loadModels(DocLanguage.getAvailableLangsNames());
        } catch (IOException exc) {
            logger.error("Language models cannot be loaded in the language detector");
            ExitProgram.errorExit();
        }
    }

    /**
     * Identify the language of some text by using the {@link OptimaizeLangDetector} for the loaded
     * languages models which are these specified in {@link DocLanguage}
     * @param text to identify tha language on it
     * @return detected language of the text
     * @throws ParsingException when language cannot be determined because of any case
     */
    public static DocLanguage identifyLanguage(String text) throws ParsingException {
        detector.reset();
        LanguageResult result = detector.detect(text);

        if (result.getConfidence() == LanguageConfidence.NONE)
            throw new ParsingException("No language detected for text: " + text);

        if (result.isLanguage(DocLanguage.EN.toString()))
            return DocLanguage.EN;
        else if (result.isLanguage(DocLanguage.PL.toString()))
            return DocLanguage.PL;
        else
            throw new ParsingException("Not supported language detected for text: " + text);
    }
}
