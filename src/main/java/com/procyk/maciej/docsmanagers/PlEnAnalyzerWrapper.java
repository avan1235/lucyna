package com.procyk.maciej.docsmanagers;

import com.procyk.maciej.properties.DocLanguage;
import com.procyk.maciej.special.UnknownLanguageException;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.AnalyzerWrapper;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static com.procyk.maciej.properties.LuceneConstants.CONTENT_EN;
import static com.procyk.maciej.properties.LuceneConstants.CONTENT_PL;

/**
 * {@link AnalyzerWrapper} which would use proper analyzer for Polish
 * and English save fields in documents when used fields names from
 * {@link com.procyk.maciej.properties.LuceneConstants}
 */
public class PlEnAnalyzerWrapper extends AnalyzerWrapper {

    private static Logger logger = LoggerFactory.getLogger(PlEnAnalyzerWrapper.class);

    private static AnalyzerFactory builder = new AnalyzerFactory();

    public PlEnAnalyzerWrapper() {
        super(Analyzer.PER_FIELD_REUSE_STRATEGY);
    }

    /**
     * Get the analyzer for specified type of field. When language not specified so
     * the field is not known then the {@link StandardAnalyzer} will be returned
     * @param fieldName name of the field to get the language for
     * @return analyzer for specified field
     */
    @Override
    protected Analyzer getWrappedAnalyzer(String fieldName) {
        Analyzer analyzer = new StandardAnalyzer();
        try {
            if (CONTENT_EN.equals(fieldName))
                analyzer = builder.getForLanguage(DocLanguage.EN);
            else if (CONTENT_PL.equals(fieldName))
                analyzer = builder.getForLanguage(DocLanguage.PL);
        } catch (UnknownLanguageException exc) {
            logger.info("Unknown Language detected. StandardAnalyzer will be used to analyze the document.");
        }
        return analyzer;
    }
}
