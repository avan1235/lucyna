package com.procyk.maciej.docsmanagers;

import com.procyk.maciej.properties.DocLanguage;
import com.procyk.maciej.special.UnknownLanguageException;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.en.EnglishAnalyzer;
import org.apache.lucene.analysis.pl.PolishAnalyzer;

/**
 * Building Analyzer based on the given {@link DocLanguage} already implemented
 * for English and Polish languages. When used in not proper way throws
 * {@link UnknownLanguageException}
 */
public class AnalyzerFactory {

    /**
     * Enum field created to use the singleton version of
     * every analyzer (because of the fact that every enum instance
     * is initialized only once and it is lazy initialized)
     */
    private enum AnalyzerForLanguage {
        POLISH_ANALYZER(new PolishAnalyzer()),
        ENGLISH_ANALYZER(new EnglishAnalyzer());

        private Analyzer getAnalyzer() {
            assert this.analyzer != null;
            return this.analyzer;
        }

        private final Analyzer analyzer;

        AnalyzerForLanguage(Analyzer analyzer) {
            this.analyzer = analyzer;
        }
    }

    /**
     * Get the proper {@link Analyzer} based on the specified language. Only Polish and English
     * Languages supported. Can be added more but also {@link DocLanguage} has to be then improved
     * @param language language to give the analyzer for
     * @return  analyzer for specified language
     * @throws UnknownLanguageException when not known language given in th parameter
     */
    public Analyzer getForLanguage(DocLanguage language) throws UnknownLanguageException {
        if (language == DocLanguage.PL)
            return AnalyzerForLanguage.POLISH_ANALYZER.getAnalyzer();
        else if (language == DocLanguage.EN)
            return AnalyzerForLanguage.ENGLISH_ANALYZER.getAnalyzer();
        else
            throw new UnknownLanguageException("Language not known in system.");
    }


}
