package com.procyk.maciej.docsmanagers;

import com.procyk.maciej.properties.SupportedFileType;
import com.procyk.maciej.special.ParsingException;

import org.apache.tika.Tika;
import org.apache.tika.exception.TikaException;
import org.apache.tika.io.TikaInputStream;

import java.io.IOException;
import java.io.InputStream;
import java.util.Objects;

/**
 * Static methods based class to analyze documents using the {@link Tika} parser.
 * Analysed documents doesn't have the limit of their size to be properly analyzed
 * and the only limitation is the memory of the program
 */
public class DocExtractor {

    private static final int DISABLE_TIKA_LIMIT = -1;
    private static final Tika tika;

    static {
        tika = new Tika();
        tika.setMaxStringLength(DISABLE_TIKA_LIMIT);
    }

    /**
     * Parse the given stream by detecting its type and when type is supported
     * for the program usage. If type should be supported then it has to be declared
     * in the {@link SupportedFileType}. Returns string based content of the parsed
     * document.
     * @param stream of the document to be parsed
     * @return parsed content of the document if document has a supported type
     * @throws ParsingException when error occurs in parsing process
     */
    public static String parseToString(InputStream stream) throws ParsingException {
        try {
            if (Objects.isNull(stream))
                throw new ParsingException("Null input stream");

            TikaInputStream tikaStream = TikaInputStream.get(stream);
            String detectedTypeString = tika.detect(tikaStream);
            SupportedFileType type = SupportedFileType.getFileType(detectedTypeString);
            if (type == SupportedFileType.NOT_SUPPORTED)
                throw new ParsingException("Not supported file type: " + detectedTypeString);

            return tika.parseToString(tikaStream);
        }
        catch (IOException | TikaException exc) {
            throw new ParsingException(exc.getMessage());
        }
    }
}
