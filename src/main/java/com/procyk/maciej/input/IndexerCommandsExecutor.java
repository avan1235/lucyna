package com.procyk.maciej.input;

import com.procyk.maciej.index.*;
import com.procyk.maciej.output.OutputFormatter;
import com.procyk.maciej.output.OutputPathsListFormatter;
import com.procyk.maciej.special.BadIndexerUsageException;

import java.util.Collection;

/**
 * Commands executor for indexer command line arguments
 * with the exact comands as specified in {@link com.procyk.maciej.core.Indexer}
 */
public class IndexerCommandsExecutor {

    private final static String PURGE = "--purge";
    private final static String ADD = "--add";
    private final static String RM = "--rm";
    private final static String REINDEX = "--reindex";
    private final static String LIST = "--list";

    private String[] commands;
    private final OutputFormatter formatter;

    public IndexerCommandsExecutor(String... commands) {
        this.formatter = new OutputPathsListFormatter();
        this.commands = commands;
    }

    /**
     * Execute the tasks specified by the given in the
     * prccess of construction of the {@link IndexerCommandsExecutor}
     * @throws BadIndexerUsageException when not know or bad format of arguments specified
     */
    public void executeTask() throws BadIndexerUsageException {
        IndexWriter indexWriter = new IndexWriter();
        if (commands.length == 0) {
            DirectoriesEventsHandler handler = new IndexWatcherAdapter();
            DirectoriesWatcher watcher = new DirectoriesWatcher(indexWriter.getIndexedDirectoriesCanonical(), handler);
            watcher.processEvents();
        }
        else if (commands.length == 1) {
            if (PURGE.equals(commands[0])) {
                indexWriter.cleanIndex();
            } else if (REINDEX.equals(commands[0])) {
                indexWriter.reindexAll();
            } else if (LIST.equals(commands[0])) {
                Collection<String> paths = indexWriter.getIndexedDirectoriesCanonical();
                System.out.println(formatter.getFormattedOutput(paths));
            }
        } else if (commands.length == 2) {
            if (ADD.equals(commands[0])) {
                indexWriter.indexIndexedDirectory(commands[1]);
            } else if (RM.equals(commands[0])) {
                indexWriter.cleanDirectory(commands[1]);
            }
        } else
            throw new BadIndexerUsageException();
    }
}
