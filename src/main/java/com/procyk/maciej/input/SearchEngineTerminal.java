package com.procyk.maciej.input;

import com.procyk.maciej.output.OutputFormatter;
import org.jline.reader.*;
import org.jline.terminal.Terminal;
import org.jline.terminal.TerminalBuilder;

import java.io.IOException;
import java.util.Collection;
import java.util.Objects;

/**
 * Terminal for the search engine which is based on the {@link Terminal}
 * and can be used with any {@link ReplExecutor} and {@link OutputFormatter}
 */
public class SearchEngineTerminal implements AutoCloseable {

    private final Terminal terminal;
    private final LineReader lineReader;
    private final ReplExecutor executor;
    private final OutputFormatter formatter;

    private static final String PROMPT_SYMBOL = "> ";

    /**
     * Create new {@link SearchEngineTerminal} with specified {@link ReplExecutor} and
     * {@link OutputFormatter}. Possible usage of some completer in the
     * @param executor to execute tasks in repl
     * @param formatter to format commands results in repl
     * @param completer to create completions for the command line
     * @param helloCompleteMessage message showed when repl starts working
     * @throws IOException when fatal error in building terminal occurred
     */
    public SearchEngineTerminal(ReplExecutor executor, OutputFormatter formatter, Completer completer, String helloCompleteMessage) throws IOException {

        if (Objects.isNull(executor))
            throw new IllegalArgumentException("Executor for command line cannot be null");

        if (Objects.isNull(formatter))
            throw new IllegalArgumentException("Formatter for command line cannot be null");

        this.executor = executor;
        this.formatter = formatter;

        terminal = TerminalBuilder.builder()
                .jna(false)
                .jansi(true)
                .build();

        lineReader = LineReaderBuilder.builder()
                .terminal(terminal)
                .completer(completer)
                .build();

        if (Objects.nonNull(helloCompleteMessage) && Objects.nonNull(completer))
            terminal.writer().println(helloCompleteMessage);
    }

    /**
     * Execute tasks in infinite loop in REPL mode of the created terminal
     */
    public void runRepl() {
        this.executor.setTerminal(this.terminal);

        while (true) {
            String currentLine;
            try {
                currentLine = this.lineReader.readLine(PROMPT_SYMBOL);
                if (currentLine.length() > 0) {
                    Collection<String> results = this.executor.execute(currentLine);
                    String outString = this.formatter.getFormattedOutput(results);
                    terminal.writer().print(outString);
                }
            } catch (UserInterruptException | EndOfFileException e) {
                break;
            }
        }
    }

    /**
     * Make terminal auto closable based on the {@link Terminal#close()} function
     * @throws IOException when error in closing terminal
     */
    @Override
    public void close() throws IOException {
        if (Objects.nonNull(this.terminal))
            this.terminal.close();
    }
}
