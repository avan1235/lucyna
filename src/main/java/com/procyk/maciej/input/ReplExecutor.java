package com.procyk.maciej.input;

import org.jline.terminal.Terminal;

import java.util.Collection;

/**
 * Executor of some repl terminal used in {@link SearchEngineTerminal}
 */
public interface ReplExecutor {

    /**
     * Execute given string command and return te collection of the
     * results of specified command. When returned collection is null
     * then no output will be displayed
     * @param command to be parsed by executor
     * @return collection of output lines of command execution
     */
    Collection<String> execute(String command);

    /**
     * Set the terminal for repl executor for better compatibility
     * @param terminal which is used in repl mode
     */
    void setTerminal(Terminal terminal);
}
