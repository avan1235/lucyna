# Lucene

Project of indexer and searcher applications based on Lucene from Apache. More application descriptions can be found on [moodle](https://moodle.mimuw.edu.pl/)

## Clarification of used assumptions for programs

* when indexer called with bad commands then nothing happens and program exits normally after logging to file
* when bad setting specified for searcher nothing happens and information is logged to file
* when searcher app called with some arguments, then they are just skipped and nothing special happens
* when indexer is watching directories it can notice the changes under the directories added using the ``--add`` option and only in them can be done some changes
* when the result of searching was found in name of file the the name of file is highlighted (all name)
* when color is off the searched phrases in context are not bold
* when starting searcher app the current settings are printed to point out the used language and change if needed
* when searching the leading and trailing spaces are cut from searched string
* it's better to use the words suggested by completer by using TAB key because it gives the user list of tokenized words from dictionary
* if any questions about using gradle or problem in project compile process pleas contact me on [hangouts](https://hangouts.google.com/group/VfxSQzis3nhp9XGQ8)
* when some problems in getting the context of search result or highlighting occured then the context is skipped and anly the path to the search result is displayed
## Deployment

 
### Prerequisites

Gradle is used for the installation of project with the project gradle wrapper so only internet access is needed to download all dependencies for project.

### Installing

In order to install the app and test it one should use gradle wrapper with command 

```
./gradlew installDist
```

and on Windows

```
./gradlew.bat installDist
```

being in the main directory where are located used scripts. Then the installed applications will be in the directory
``` 
./build/install/Indexer/bin
```

and Indexer can be used with special arguments or without them.

#### Examples (from Linux console)
For example to run indexer app with some arguments
``` 
cd ./build/install/Indexer/bin
./Indexer --add /home/userName/indexedDir
./Indexer --list
1 path in index
/home//userName/indexedDir
./Indexer --purge
./Indexer --list
0 paths in index
```
and to start searching using the searcher app:
``` 
cd ./build/install/Indexer/bin
./Searcher
```
#### Clean install

To do clean install, type 
```
./gradlew clean
```
before installation process

## Built With

* [Lucene](http://lucene.apache.org/) - Files indexer and searcher core
* [Tika](https://tika.apache.org/) - Files parser and language detector
* [JLine3](https://github.com/jline/jline3) - Terminal handler
* [Gradle](https://gradle.org/) - Dependency Management


## Authors

* **Maciej Procyk** - University of Warsaw ([gitlab account](http://gitlab.com/avan1235))
